from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from .serializers import UploadSerializer,MultipleFilesUploadSerializer,TrainSerializer
from PIL import Image

import argparse
import sys
import tensorflow as tf
import pickle
from scipy import misc
import cv2
import numpy as np
from facenet import facenet,detect_face
import tensorflow.compat.v1 as tf 
import os
import time
from facenet.utils import *
import sklearn
from skimage.transform import rescale, resize
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from rest_framework.decorators import api_view


from skimage.io import imsave
import math

from sklearn.svm import SVC

from facenet.align.yolo.yolo import *


import random
from time import sleep
from tensorflow.python.framework.ops import disable_eager_execution


class TrainViewSet(ViewSet):


    @api_view(['GET'])
    def apiOverview(request):

        api_urls = {
        'Align Pictures': '/align/',
        'Train a new Model': '/train/',
        'Test a Model': '/file/',
        
    }

        return Response(api_urls)

    @api_view(['POST'])
    def modelCreate(request):
    

        serializer = TrainSerializer(data=request.data)
       
        if serializer.is_valid():
            serializer.save()
            class_name=serializer.data["class_name"]

            with tf.Session() as sess:
                
                datadir = 'facenet/align/aligned_faces/'
                dataset = facenet.get_dataset(datadir)
                paths, labels = facenet.get_image_paths_and_labels(dataset)
                print('Number of classes: %d' % len(dataset))
                print('Number of images: %d' % len(paths))

                print('Loading feature extraction model')
                modeldir = 'facenet/models/20180408-102900/20180408-102900.pb'
                facenet.load_model(modeldir)

                images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
                embedding_size = embeddings.get_shape()[1]

                # Run forward pass to calculate embeddings
                print('Calculating features for images')
                batch_size = 1000
                image_size = 160
                nrof_images = len(paths)
                nrof_batches_per_epoch = int(math.ceil(1.0 * nrof_images / batch_size))
                emb_array = np.zeros((nrof_images, embedding_size))
                for i in range(nrof_batches_per_epoch):
                    start_index = i * batch_size
                    end_index = min((i + 1) * batch_size, nrof_images)
                    paths_batch = paths[start_index:end_index]
                    images = facenet.load_data(paths_batch, False, False, image_size)
                    feed_dict = {images_placeholder: images, phase_train_placeholder: False}
                    emb_array[start_index:end_index, :] = sess.run(embeddings, feed_dict=feed_dict)

                classifier_filename = 'facenet/myclassifier/'+class_name+'.pkl'
                classifier_filename_exp = os.path.expanduser(classifier_filename)

                # Train classifier
                print('Training classifier')
                model = SVC(kernel='linear', probability=True)
                model.fit(emb_array, labels)

                # Create a list of class names
                class_names = [cls.name.replace('_', ' ') for cls in dataset]

                # Saving classifier model
                with open(classifier_filename_exp, 'wb') as outfile:
                    pickle.dump((model, class_names), outfile)
                print('Saved classifier model to file "%s"' % classifier_filename_exp)
                
                return Response('The model has been successfuly created')
            return Response('Error while creating the model')
    
            
    

   
    
