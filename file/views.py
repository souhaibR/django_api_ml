from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from .serializers import UploadSerializer,MultipleFilesUploadSerializer
from PIL import Image




import tensorflow as tf
import pickle
from scipy import misc
import cv2
import numpy as np
from facenet import facenet,detect_face
import tensorflow.compat.v1 as tf 
import os
import time
from facenet.utils import *
import sklearn
from skimage.transform import rescale, resize
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from .models import emotion
from rest_framework.decorators import api_view

# ViewSets define the view behavior.
class UploadViewSet(ViewSet):
    
    print('Creating networks and loading parameters')
    serializer_class =UploadSerializer
    

  
    def list(self, requests):
        return Response("GET API")

    
    def create(self,request):
    
        
       
        print('tensorflow version ',tf.__version__)
        net = cv2.dnn.readNetFromDarknet('facenet/yolo_cfg/yolov3-face.cfg', 'facenet/yolo_weights/yolov3-wider_16000.weights')
        net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
        net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        
       
        file_uploaded = request.FILES.get('Upload_Image')
        class_name=  request.data['Class_Name']

        content_type = file_uploaded.content_type
       
        img = Image.open(file_uploaded)
        print('image: ',img)
        print('class name: ',class_name)

        frame = np.array(img) 

        print('Creating networks and loading parameters')
        with tf.Graph().as_default():
            gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
            sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
            with sess.as_default():
                

                minsize = 20  # minimum size of face
                threshold = [0.6, 0.7, 0.7]  # three steps's threshold
                factor = 0.709  # scale factor
                margin = 44
                frame_interval = 3
                batch_size = 1000
                image_size = 182
                input_image_size = 160

            
                print('Loading feature extraction model')
                modeldir = 'facenet/models/20180408-102900/20180408-102900.pb'

                facenet.load_model(modeldir)
               

                images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
                embedding_size = embeddings.get_shape()[1]

                classifier_filename = 'facenet/myclassifier/'+class_name
                classifier_filename_exp = os.path.expanduser(classifier_filename)
                with open(classifier_filename_exp, 'rb') as infile:
                    (model, class_names) = pickle.load(infile)
                    print('load classifier file-> %s' % classifier_filename_exp)
                    print('class_names',class_names)
            
            
            
                c = 0
                print('Start Recognition!')
                prevTime = 0
               

                curTime = time.time()    # calc fps
                timeF = frame_interval


                

                if (c % timeF == 0):
                    find_results = []

                    if frame.ndim == 2:
                       frame = facenet.to_rgb(frame)
                    frame = frame[:, :, 0:3]

                
                    # Use YOLO to get bounding boxes
                    blob = cv2.dnn.blobFromImage(frame, 1 / 255, (IMG_WIDTH, IMG_HEIGHT), [0, 0, 0], 1, crop=False)

                    # Sets the input to the network
                    net.setInput(blob)

                    # Runs the forward pass to get output of the output layers
                    outs = net.forward(get_outputs_names(net))

                    # Remove the bounding boxes with low confidence
                    bounding_boxes = post_process(frame, outs, CONF_THRESHOLD, NMS_THRESHOLD)
                    nrof_faces = len(bounding_boxes)
                    print('Number of faces captured : ',nrof_faces )
               



               
                if nrof_faces > 0:
                    print('Bounding Boxes: ',bounding_boxes)

                    img_size = np.asarray(frame.shape)[0:2]

                    bb = np.zeros((nrof_faces,4), dtype=np.int32)

                    emotion_dic={}
                    recognition_probabilities_dict={}
                    recognition_classes_dict={}
                    
                    for i in range(nrof_faces):
                       
                        
                        emb_array = np.zeros((1, embedding_size))

                        bb[i][0] = bounding_boxes[i][0]
                        bb[i][1] = bounding_boxes[i][1]
                        bb[i][2] = bounding_boxes[i][2]
                        bb[i][3] = bounding_boxes[i][3]

                        if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= len(frame[0]) or bb[i][3] >= len(frame):
                            print('face is inner of range!')
                            continue

                        cropped = (frame[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])

                        model_emotions=tf.keras.models.load_model('emotion_recognition\FER_model_v2.h5')
                        gray_image_for_emotion = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                        cropped_for_emotion = (gray_image_for_emotion[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2]])
                        cropped_for_emotion=cv2.resize(cropped_for_emotion,(48,48))

                        img_pixels_for_emotion = image.img_to_array(cropped_for_emotion)
                        img_pixels_for_emotion = np.expand_dims(img_pixels_for_emotion, axis = 0)
                        img_pixels_for_emotion /= 255

                        predictions_for_emotion = model_emotions.predict(img_pixels_for_emotion)

                        

                        #find max indexed array
                        max_index_for_emotion = np.argmax(predictions_for_emotion[0])

                        emotions = ('angry','fear','happy','neutral','sad','surprise')
                        predicted_emotion = emotions[max_index_for_emotion]

                        try:
                            e = emotion.objects.get(name=predicted_emotion)
                            num = e.number
                            num = num + 1
                            e.number = num
                        except:
                            e = emotion(name=predicted_emotion, number=1)
                        e.save()

                        emotion_dic[i+1]=predicted_emotion 
                        



                        print("{0} {1} {2} {3}".format(bb[i][0], bb[i][1], bb[i][2], bb[i][3]))
                        cropped = facenet.flip(cropped, False)
                        scaled = (resize(cropped, (image_size, image_size)))
                        scaled = cv2.resize(scaled, (input_image_size,input_image_size),
                                            interpolation=cv2.INTER_CUBIC)

                        scaled = facenet.prewhiten(scaled)
                        scaled_reshape = (scaled.reshape(-1,input_image_size,input_image_size,3))
                        feed_dict = {images_placeholder: scaled_reshape, phase_train_placeholder: False}

                        emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)

                        predictions = model.predict_proba(emb_array)
                        print('predictions :',predictions)

                        predictions_class=model.predict(emb_array)
                        print('predict class',predictions_class)
                        best_class_indices = np.argmax(predictions, axis=1)
                        best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
                        frame = np.array(frame)
                        
                        cv2.rectangle(frame, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)
                        text_x = bb[i][0]
                        text_y = bb[i][3] + 20
                        print('probabilities: ',best_class_probabilities[0])
                       

                        if class_names[int(predictions_class)]!="Unknown"   and best_class_probabilities[0]>0.90 :
                            print('Hello ',class_names[int(predictions_class)])
                            recognition_classes_dict[i+1]=class_names[int(predictions_class)]
                            recognition_probabilities_dict[i+1]= best_class_probabilities[0]

                        else :
                           
                            recognition_classes_dict[i+1]="Unknown"
                            recognition_probabilities_dict[i+1]= 1-best_class_probabilities[0]

                    
                    proba_display="Probabilities to be "+class_name[0:-4]
                    
                    return Response({"mode": img.mode, "size": img.size, "format": img.format,"Number of faces ":nrof_faces,"Recognition": recognition_classes_dict, proba_display:recognition_probabilities_dict,"Emotions":emotion_dic})

                else:
                    print('Unable to align')
                    pasdeface="No face"
                    return Response({"mode": img.mode, "size": img.size, "format": img.format,"Number of faces ":nrof_faces})
            
    
    
    @api_view(['GET'])
    def stat(request):

        emotions = emotion.objects.all()
        for e in emotions:
            print('emotion:')
            print(e.name)
            print('number:')
            print(e.number)
        #angry
        try:
            angry=emotion.objects.get(name='angry')

        except:
            angry=emotion(name='angry',number=0)

        #happy
        try:
            happy=emotion.objects.get(name='happy')
        except:
            happy=emotion(name='happy',number=0)

        #neutral
        try:
            neutral=emotion.objects.get(name='neutral')
        except:
            neutral=emotion(name='neutral',number=0)

        #sad
        try:
            sad=emotion.objects.get(name='sad')
        except:
            sad=emotion(name='sad',number=0)

        #surprise
        try:
            surprise=emotion.objects.get(name='surprise')
        except:
            surprise=emotion(name='surprise',number=0)
        #fear
        try:
            fear=emotion.objects.get(name='fear')
        except:
            fear=emotion(name='fear',number=0)


        return Response({angry.name:angry.number,happy.name:happy.number,surprise.name:surprise.number,neutral.name:neutral.number,sad.name:sad.number,fear.name:fear.number})

    
    @api_view(['GET'])
    def empty_stat(request):
        emotions = emotion.objects.all()
        
        for e in emotions:
            print('emotions:', e)
            e.number=0
            e.save()
        #angry
        try:
            angry=emotion.objects.get(name='angry')

        except:
            angry=emotion(name='angry',number=0)

        #happy
        try:
            happy=emotion.objects.get(name='happy')
        except:
            happy=emotion(name='happy',number=0)

        #neutral
        try:
            neutral=emotion.objects.get(name='neutral')
        except:
            neutral=emotion(name='neutral',number=0)

        #sad
        try:
            sad=emotion.objects.get(name='sad')
        except:
            sad=emotion(name='sad',number=0)

        #surprise
        try:
            surprise=emotion.objects.get(name='surprise')
        except:
            surprise=emotion(name='surprise',number=0)
        #fear
        try:
            fear=emotion.objects.get(name='fear')
        except:
            fear=emotion(name='fear',number=0)

        return Response({angry.name:angry.number,happy.name:happy.number,surprise.name:surprise.number,neutral.name:neutral.number,sad.name:sad.number,fear.name:fear.number})

        

        


    